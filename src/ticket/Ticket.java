package ticket;

public class Ticket {
	private int ticketid;
	private int price;
	private static  int availableTickets;
	
	
	public static int getAvailableTickets() {
		return availableTickets;
	}
	
	public static void setAvailableTickets(int availticket) {
		
		if(availticket>0) {
			availableTickets = availticket;
		}
	}
	
	//for ticket id
	public int getTicketid() {
		return ticketid;
	}
	
	public void setTicketid(int tic) {
		
		ticketid = tic;
		
	}
	
	//for price
	public int getPrice() {
		return price;
	}
	
	public void setPrice(int pri) {
		
		price = pri;
		
	}
	
	
	public int calculateTicketCost(int nooftickets) {
		
	int avail = getAvailableTickets();
	avail -=nooftickets;
	setAvailableTickets(avail);
	
	int price = getPrice();
	if(avail > nooftickets ) {
		price = nooftickets * price;
		return price;
	} 
	else return -1;
	
	
	
	
}
	
	
}

