package student;
import java.util.Scanner;

public class Student {
	private int studentId;
	private String studentName;
	private String studentAddress;
	private String collegeName;
	
	
	public Student(int studentId,String studentName,String studentAddress ,String collegeName) {
		this.studentId = studentId;
		this.studentName = studentName;
		this.studentAddress = studentAddress;
		this.collegeName = collegeName;
	}
	
	public Student(int studentId,String studentName,String studentAddress) {
		this.studentId = studentId;
		this.studentName = studentName;
		this.studentAddress = studentAddress;
		this.collegeName = "NIT";
	}
	
	
	
	
	
	
	
	
	public int getstudentId() {
		return studentId;
	}
	
	
	
	public String getstudentName() {
		return studentName;
	}
	
	
	

	public String getstudentAddress() {
		return studentAddress;
	}
	
	
	
	public String getcollegeName() {
		return collegeName;
	}
	
	
}
