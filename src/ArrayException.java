import java.util.InputMismatchException;
import java.util.Scanner;

public class ArrayException {
	
	public String getPriceDetails() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of elements in the array");
		int a=sc.nextInt();
		
	
		
		int[] b = new int[a];
		System.out.println("Enter the price details");
		for(int i = 0 ; i<a ; i++) {
			try {
			b[i] = sc.nextInt();
			}
			catch(InputMismatchException e){
				return "Input was not in the correct format";
			}
		}
		System.out.println("Enter the index of the array element you want to access");
		int c = sc.nextInt();
		
		try {
			String g = String.valueOf(b[c]);
			return "The array element is " +g ;
		}
		catch(ArrayIndexOutOfBoundsException e) {
			return "Array index is out of range";
					
		}
		
	}

	

	public static void main(String[] args) {
		ArrayException a1 = new ArrayException();
		String h = a1.getPriceDetails();
		System.out.println(h);
	}

}
