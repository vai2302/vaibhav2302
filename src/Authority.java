import java.util.Scanner;
import java.util.regex.*;

public class Authority {

	public static void main(String[] args) {
		String regex = "^[A-Za-z ]+$";
		Pattern p = Pattern.compile(regex);
		 Scanner sc = new Scanner(System.in);
	      System.out.println("Inmate's name:");
	      String str1 = sc.nextLine();
	     
	      System.out.println("Inmate's father's name:");
	      String str2 = sc.nextLine();
	     
	      Matcher m = p.matcher(str1);
	      Matcher f = p.matcher(str2);
	      if(m.matches()==false) {
	    	  System.out.println("Invalid name");
	      }
	      else if(f.matches()==false) {
	    	  System.out.println("Invalid father's name");
	      }
	      else {
	    	 String s1 = str1.toUpperCase();
	    	 String s2 = str2.toUpperCase();
	    	 
	    	 String s3 = s1.concat(" "+s2);
	    	 System.out.println(s3);
	    	 
	      }
	      
		   sc.close();
	   }
	}

