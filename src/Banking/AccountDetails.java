package Banking;
import java.util.Scanner;

public class AccountDetails {

	
		public Account getAccountDetails() {
			 Scanner sc = new Scanner(System.in);
			 System.out.println("Enter account id:");
			 int a = sc.nextInt();
			 
			 System.out.println("Enter account type:");
			 String str = sc.next();
			 
			 boolean valid = false;
			 int b = 0;
			
			 while(!valid) {
				 System.out.println("Enter balance");
				 b = sc.nextInt();
				 if(b<=0) {
				 System.out.println("Balance should be positive");
				 }
				 else {
					 valid= true;
				 }
				
			 }
			 
			Account account = new Account();
			account.setaccountId(a);
			account.setaccountType(str);
			account.setbalance(b);			 
		
			
			return account;
		}
		
		public int getWithdrawAmount() {
			 Scanner sc = new Scanner(System.in);
			 boolean valid2 = false;
			 int withdraw = 0;
			 
			 while(!valid2) {
				 System.out.println("Enter amount to be withdrawn:");
				 withdraw = sc.nextInt();
				 
				 if(withdraw <= 0 ) {
					 System.out.println("Amount should be positive");
				 }
				 else {
					 valid2=true;
				 }
			 }
			 
			 return withdraw;
		}
		
		public static void main(String[] args) {
			AccountDetails accountdetails = new AccountDetails();
			Account a = new Account();
			accountdetails.getAccountDetails();
			int withdraw = accountdetails.getWithdrawAmount();
			boolean withdrawStatus = a.withdraw(withdraw);
			
		}
	
	}


