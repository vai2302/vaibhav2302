import java.util.*;

public class UniqueWords
{
    public static void main(String[] args) {
		
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Student's Article");
		String str = sc.nextLine().toLowerCase();
		
		String[] words  = str.split("[ ,;:.?!]+");
		int  length = words.length;
		System.out.println("Number of words " + length);
		
		Set<String> uniq = new TreeSet<String>(Arrays.asList(words));
		System.out.println("Number of unique words " + uniq.size());
		
	
		System.out.println("The words are");
	    int c =0;
		for(String s:uniq) {
		
			
			System.out.println(++c + ". " + s);
		}
		
	}

         
}
