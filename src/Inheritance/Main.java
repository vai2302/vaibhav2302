package Inheritance;
import java.util.Scanner;
public class Main {
	public static Hosteller getHostellerDetails() {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter Details:");
		
		System.out.println("Student Id");
		int a = sc.nextInt();
		
		System.out.println("Student Name");
		String b = sc.next();
		
		System.out.println("Department Id");
		int c = sc.nextInt();
		
		System.out.println("Gender");
		String d = sc.next();
		
		System.out.println("Phone Number");
		String e = sc.next();
		
		System.out.println("Hostel Name");
		String f = sc.next();
		
		System.out.println("Room Number");
		int g = sc.nextInt();
		
	
		
		
		Hosteller h1 = new Hosteller();
		
	
		h1.setStudentId(a);
		h1.setName(b);
		h1.setDepartmentId(c);
		h1.setGender(d);
		h1.setPhone(e);
		h1.setHostelName(f);
		h1.setRoomNumber(g);
		
		return h1;
	
		
		
		
		
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Hosteller hostel = getHostellerDetails();
		
		
		System.out.println("Modify Room Number(Y/N)");
		boolean update = sc.next().equals("Y");
		
		if(update) {
			System.out.println("New Room Number");
			int r = sc.nextInt();
			hostel.setRoomNumber(r);
		}
		
		System.out.println("Modify phone Number(Y/N)");
		boolean update1 = sc.next().equals("Y");
		
		if(update1) {
			System.out.println("New Room Number");
			String p = sc.next();
			hostel.setPhone(p);
		}
		
		System.out.println("The Student Details:");
		System.out.print(hostel.getStudentId()+" "+hostel.getName()+" "+hostel.getDepartmentId()+" "+hostel.getGender()+" "+hostel.getPhone()+" "+hostel.getHostelName()+" "+hostel.getRoomNumber());
		
	}

}
