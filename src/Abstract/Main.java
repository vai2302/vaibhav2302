package Abstract;
public class Main{
    
    public static void main(String args[]){
        Customer cust = new Customer(101 , "jbj", "ugwdu" );
        SavingsAccount sav = new SavingsAccount(22121 , cust ,  10000 , 500);
        
        boolean y = sav.withdraw(9000);
        
        if(y){
            System.out.println(sav.getBalance());
        }
        
    }
}
